
# 801 - PostgreSQL - Notas de aula

Mais informações em https://tureba.org/postgresql.html

## Prática 1: Instalação do PostgreSQL

Os passos a seguir mostram a instalação do PostgreSQL versão 12. Quando precisar instalar outra versão, altere o número nos passos. Por exemplo 12 => 11.


### Instalação em Debian e Ubuntu

Siga os passos em https://apt.postgresql.org/

1. Importe a chave do repositório (como root)

    ```shell
    apt install curl ca-certificates gnupg
    curl https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
    ```

2. Adicione o repositório (como root)

    ```shell
    echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list
    ```

3. Busque o repositório (como root)

    ```shell
    apt update
    ```

4. Instale o PostgreSQL 12 (como root)

    ```shell
    apt install postgresql-12
    ```

5. (Extra e opcional) Remova o cluster inicial do PostgreSQL 12 (como root ou postgres)

    ```shell
    pg_dropcluster 12 main --stop
    ```

### Instalação em CentOS, RHEL, Oracle Linux e outros

Siga os passos em https://yum.postgresql.org/

1. Instale o repositório (como root)

    ```shell
    rpm -iv https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm
    ```

2. Instale o PostgreSQL 12 (como root)

    ```shell
    yum install postgresql12-server
    ```


## Prática 2: Inicialização do cluster do PostgreSQL

### Inicialização em Debian e Ubuntu (como root ou postgres)

```shell
pg_createcluster 12 main
```

### Inicialização em Centos, RHEL, Oracle Linux e outros (como postgres)

```shell
/usr/pgsql-12/bin/postgresql-12-setup initdb
```


## Prática 3: Iniciando o serviço do PostgreSQL

### Iniciar o serviço em Debian e Ubuntu (como root)

```shell
systemctl start postgresql@12-main.service
```

### Iniciar o serviço em CentOS, RHEL, Oracle Linux e outros (como root)

```shell
systemctl start postgresql-12.service
```


## Prática 4: Parando o serviço do PostgreSQL

### Parar o serviço em Debian e Ubuntu (como root)

```shell
systemctl stop postgresql@12-main.service
```

### Parar o serviço em CentOS, RHEL, Oracle Linux e outros (como root)

```shell
systemctl stop postgresql-12.service
```


## Prática 5: Reiniciando o serviço do PostgreSQL

### Reiniciar o serviço em Debian e Ubuntu (como root)

```shell
systemctl restart postgresql@12-main.service
```

### Reiniciar o serviço em CentOS, RHEL, Oracle Linux e outros (como root)

```shell
systemctl restart postgresql-12.service
```


## Prática 6: Recarregando as configurações no serviço do PostgreSQL

### Método 1: Recarregar as configurações no serviço em Debian e Ubuntu (como root)

```shell
systemctl reload postgresql@12-main.service
```

### Método 1: Recarregar as configurações no serviço em CentOS, RHEL, Oracle Linux e outros (como root)

```shell
systemctl reload postgresql-12.service
```

### Método 2: Recarregar pelo psql

```sql
select pg_reload_conf();
```


## Prática 7: Conectar no PostgreSQL pelo psql

Usando argumentos padrão, ou seja, conectando localmente pela porta padrão, com o usuário do sistema operacional e com o banco igual ao usuário:

```shell
psql
```

Argumentos completos:

```shell
psql -p <porta> -h <host> -U <usuario> <database>
```


## Prática 8: Alterar uma configuração do PostgreSQL

### Método 1: Editar arquivo postgresql.conf

1. Abra o arquivo /etc/postgresql/12/main/postgresql.conf (Debian e Ubuntu) ou o arquivo /var/lib/pgsql/12/data/postgresql.conf (CentOS, RHEL, Oracle Linux e outros) com um editor de texto

2. Localize a linha da configuração

3. Edite a linha substituindo o valor anterior para o valor novo

4. Descomente a linha, caso esteja comentada

5. Salve as alterações

6. Recarregue as configurações (prática 6)

### Método 2: ALTER SYSTEM (por psql)

Forma geral:

```sql
ALTER SYSTEM SET <conf> TO '<valor>';
```

Exemplo alterando a configuração _listen\_addresses_:

```sql
ALTER SYSTEM SET listen_addresses TO '*';
```

Ao final, é necessário recarregar as configurações (prática 6);


## Prática 9: Adicionar uma regra de autenticação

1. Abra o arquivo /etc/postgresql/12/main/pg_hba.conf (Debian e Ubuntu) ou o arquivo /var/lib/pgsql/12/data/pg_hba.conf (CentOS, RHEL, Oracle Linux e outros) com um editor de texto

2. Adicione uma linha ao final do arquivo (ou em outra posição para mudar a precedência) com a regra

3. Salve as alterações

4. Recarregue as configurações (prática 6)

Exemplos de regras comuns:

* Aceitar todas as conexões de uma subrede confiável

    `host all all 192.168.56.0/24 trust`

* Aceitar todas as conexões de replicação da mesma rede confiável

    `host replication all 192.168.56.0/24 trust`

* Aceitar conexões locais quando o usuário do sistema operacional for igual ao do banco

    `local all all peer`

    `host all all 127.0.0.1/32 ident`

    `host all all ::1/128 ident`

* Autenticar a conexão na mesma rede com senha do usuário do banco pelo método scram-sha-256

    `host all all samenet scram-sha-256`


## Prática 11: Configurando um servidor PostgreSQL para receber conexões externas

1. Use a prática 8 para alterar a configuração

    `listen_addresses = '*'`

2. Use a prática 10 para aceitar conexões da rede local

    `host all all samenet trust`

3. Se o servidor for usado para replicar, use a prática 10 para aceitar também conexões de replicação

    `host replication all samenet trust`

3. Use a prática 5 para reiniciar o serviço


## Prática 12: Extraindo um dump por pg\_dump

1. Use a prática 11 para garantir que o servidor aceita conexões a partir da máquina que fará o dump

2. Faça o dump

    Adapte os parâmetros -p, -h e -U para apontar para o servidor ou omita eles para usar os valores padrão.

    Adapte o parâmetro banco\_de\_dados para apontar para o banco de dados desejado.

    Excolha um nome do arquivo de saída, por exemplo dump\_do\_banco\_de\_dados.sql.

    ```shell
    pg_dump -p <porta> -h <host> -U <usuario> banco_de_dados >dump_do_banco_de_dados.sql
    ```

Exemplo de um dump do banco de dados postgres local na porta padrão 5432, como usuário postgres:

```shell
pg_dump >dump_do_postgres.sql
```


## Prática 13: Fazendo um backup por pg\_basebackup

1. Use a prática 11 para garantir que o servidor aceita conexões (inclusive de replicação) a partir da máquina que fará o backup

2. Faça o backup

    Adapte os parâmetros -p, -h e -U para apontar para o servidor ou omita eles para usar os valores padrão.

    Escolha um diretório de destino para o novo backup, por exemplo novo\_backup.

    ```shell
    pg_basebackup -p <porta> -h <host> -U <usuario> -P -D novo_backup
    ```

3. (Apenas Debian e Ubuntu) Copie também o diretório contendo as configurações do cluster (por exemplo, /etc/postgresql/12/main) para um outro diretório ao lado do novo\_backup.

Exemplo de um backup do cluster local na porta padrão 5432, como usuário postgres:

```shell
pg_basebackup -P -D novo_backup
```

## Prática 14: Arquivando WAL

Obs.: Esta prática usa `scp`, que é ditático mas não é recomendado para ambientes de produção.

Adapte o hostname `<remoto>` para apontar para o local onde os segmentos de WAL serão arquivados, adicionando possivelmente -p `<porta>` ou -i `<chave>` caso seja necessário.

Adapte também o caminho para o local onde os segmentos de WAL serão arquivados, por exemplo em `archive`.

1. Garanta que o usuário do banco (normalmente postgres) consegue estabelecer conexões sem senha para a máquina de arquivamento, testando...

    ```shell
    ssh <remoto> hostname
    ```

    ...deve retornar o nome da máquina sem pedir senha para o usuário.

2. Use a prática 8 para alterar duas configurações

    `archive_mode = on`

    `archive_command = 'scp %f <remoto>:archive/%p'`

3. Use a prática 5 para reiniciar o serviço






